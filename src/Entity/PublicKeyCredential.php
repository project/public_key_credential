<?php

declare(strict_types = 1);

namespace Drupal\public_key_credential\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\public_key_credential\PublicKeyCredentialInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the public key credential entity class.
 *
 * @ContentEntityType(
 *   id = "public_key_credential",
 *   label = @Translation("Public Key Credential"),
 *   label_collection = @Translation("Public Key Credentials"),
 *   label_singular = @Translation("public key credential"),
 *   label_plural = @Translation("public key credentials"),
 *   label_count = @PluralTranslation(
 *     singular = "@count public key credentials",
 *     plural = "@count public key credentials",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\public_key_credential\PublicKeyCredentialListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\public_key_credential\PublicKeyCredentialAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\public_key_credential\Form\PublicKeyCredentialForm",
 *       "edit" = "Drupal\public_key_credential\Form\PublicKeyCredentialForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "public_key_credential",
 *   admin_permission = "administer public_key_credential",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/public-key-credential",
 *     "add-form" = "/public-key-credential/add",
 *     "canonical" = "/public-key-credential/{public_key_credential}",
 *     "edit-form" = "/public-key-credential/{public_key_credential}/edit",
 *     "delete-form" = "/public-key-credential/{public_key_credential}/delete",
 *     "delete-multiple-form" = "/admin/content/public-key-credential/delete-multiple",
 *   },
 * )
 */
final class PublicKeyCredential extends ContentEntityBase implements PublicKeyCredentialInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!is_int($this->getOwnerId())) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the public key credential was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the public key credential was last edited.'));

    return $fields;
  }

}
