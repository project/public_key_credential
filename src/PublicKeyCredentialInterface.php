<?php

declare(strict_types = 1);

namespace Drupal\public_key_credential;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a public key credential entity type.
 */
interface PublicKeyCredentialInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
