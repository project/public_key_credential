<?php

declare(strict_types = 1);

namespace Drupal\public_key_credential;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the public key credential entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class PublicKeyCredentialAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view public_key_credential', 'administer public_key_credential'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit public_key_credential', 'administer public_key_credential'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete public_key_credential', 'administer public_key_credential'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create public_key_credential', 'administer public_key_credential'], 'OR');
  }

}
